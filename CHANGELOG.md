
## 0.1.9 [11-26-2019]

* Patch/add ssl

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!3

---

## 0.1.8 [11-26-2019]

* Patch/add ssl

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!3

---

## 0.1.7 [11-25-2019]

* improve README.

See merge request itentialopensource/adapters/persistence/adapter-db_mysql!1

---

## 0.1.6 [11-21-2019]

* Bug fixes and performance improvements

See commit 469d9ab

---

## 0.1.5 [11-21-2019]

* Bug fixes and performance improvements

See commit 6b15e9a

---

## 0.1.4 [11-21-2019]

* Bug fixes and performance improvements

See commit a6d2f9a

---
